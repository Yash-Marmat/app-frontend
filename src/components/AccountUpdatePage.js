import React, { useState, useEffect } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { getUserAccount, logout, userAccountUpdate } from '../actions/userActions'
import { UPDATE_USER_ACCOUNT_RESET } from '../constants'


function AccountUpdatePage({ history }) {
    const dispatch = useDispatch()

    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [username, setUsername] = useState("")
    const [email, setEmail] = useState("")
    const [phone_number, setPhoneNumber] = useState("")
    const [address, setAddress] = useState("")
    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")

    // reducer
    const userLoginReducer = useSelector(state => state.userLoginReducer)
    const { userInfo } = userLoginReducer

    // reducer
    const getAccountReducer = useSelector(state => state.getAccountReducer)
    const { user, error } = getAccountReducer

    // reducer
    const updateAccountReducer = useSelector(state => state.updateAccountReducer)
    const { success } = updateAccountReducer

    useEffect(() => {
        if (!userInfo) {
            history.push("/login")
        }
        dispatch(getUserAccount('account')) // extracting the info first (get request)
    }, [dispatch, history, userInfo, success])

    const onSubmit = (e) => {
        e.preventDefault()

        const updatedfirstName = firstName === "" ? user.firstName : firstName
        const updatedLastName = lastName === "" ? user.lastName : lastName
        const updatedUsername = username === "" ? user.username : username
        const updatedEmail = email === "" ? user.email : email
        const updatedPhoneNumber = phone_number === "" ? user.phone_number : phone_number
        const updatedAddress = address === "" ? user.address : address

        if (password !== confirmPassword) {
            alert("Passwords do not match")
        } else {
            dispatch(userAccountUpdate({
                'id': user.id,
                'firstName': updatedfirstName,
                'lastName': updatedLastName,
                'username': updatedUsername,
                'email': updatedEmail,
                'phone_number': updatedPhoneNumber,
                'address': updatedAddress,
                'password': password,
            }))
            alert("Account Updated Successfully")
            history.push("/")
        }

    }

    // token expiration
    const erroHandler = () => {
        dispatch({ type: UPDATE_USER_ACCOUNT_RESET })
        dispatch(logout())
        window.location.reload();
    }

    return (
        <div>
            {error ? erroHandler() : ""}
            <div className="mx-auto" style={{ "width": "300px" }}>
                <h3>Update User Profile</h3>
            </div>
            <Row className='justify-content-md-center'>

                <Col xs={12} md={6}>
                    <Form onSubmit={onSubmit}>

                        <Form.Group controlId='firstName'>
                            <Form.Label>
                                First Name
                        </Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="update your first name"
                                defaultValue={user.firstName}
                                onChange={(e) => setFirstName(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='lastName'>
                            <Form.Label>
                                Last Name
                        </Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="update your last name"
                                defaultValue={user.lastName}
                                onChange={(e) => setLastName(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='email'>
                            <Form.Label>
                                Email Address
                        </Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="update your email"
                                defaultValue={user.email}
                                onChange={(e) => setEmail(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='phoneNumber'>
                            <Form.Label>
                                Phone Number
                        </Form.Label>
                            <Form.Control
                                type="phone"
                                placeholder="update your phone number"
                                defaultValue={user.phone_number}
                                onChange={(e) => setPhoneNumber(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='address'>
                            <Form.Label>
                                Address
                        </Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="update your address"
                                defaultValue={user.address}
                                onChange={(e) => setAddress(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>


                        <Form.Group controlId='password'>
                            <Form.Label>
                                Reset Password
                        </Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="new password"
                                onChange={(e) => setPassword(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='passwordConfirm'>
                            <Form.Label>
                                Confirm New Password
                        </Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="confirm new password"
                                onChange={(e) => setConfirmPassword(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Button type="submit" className="mb-2" variant='primary'>Save Changes</Button>
                    </Form>
                </Col>
            </Row>
        </div>
    )
}

export default AccountUpdatePage
