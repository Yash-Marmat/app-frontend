import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Container, Row, Col, Modal, Button, Form } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { getUserAccount, logout } from '../actions/userActions'
import { UPDATE_USER_ACCOUNT_RESET } from '../constants'


function AccountPage({ history }) {
    const dispatch = useDispatch()

    // reducer
    const userLoginReducer = useSelector(state => state.userLoginReducer)
    const { userInfo } = userLoginReducer

    // reducer
    const getAccountReducer = useSelector(state => state.getAccountReducer)
    const { user, error } = getAccountReducer

    useEffect(() => {
        if (!userInfo) {
            history.push("/login")
        }

        else {
            if (userInfo.id !== user.id) {
                dispatch({ type: UPDATE_USER_ACCOUNT_RESET })
            }
            dispatch(getUserAccount('account'))
        }
    }, [history, userInfo])

    // token expiration
    const erroHandler = () => {
        dispatch({ type: UPDATE_USER_ACCOUNT_RESET })
        dispatch(logout())
        window.location.reload();
    }

    return (
        <div>
            {error ? erroHandler() : ""}
            <Container>
                <div className="mx-auto" style={{ "width": "200px" }}>
                    <h3>User Profile</h3>
                </div>
                <Row className="mr-6 mb-2 border border-dark">
                    <Col xs={2} className="p-3 bg-info text-white">First Name:</Col>
                    <Col className="p-3 text-capitalize">{user.firstName}</Col>
                </Row>
                <Row className="mr-6 mb-2 border border-dark">
                    <Col xs={2} className="p-3 bg-info text-white">Last Name:</Col>
                    <Col className="p-3 text-capitalize">{user.lastName}</Col>
                </Row>
                <Row className="mr-6 mb-2 border border-dark">
                    <Col xs={2} className="p-3 bg-info text-white">Username:</Col>
                    <Col className="p-3">{user.username}</Col>
                </Row>
                <Row className="mb-2 border border-dark">
                    <Col xs={2} className="p-3 bg-info text-white">Email:</Col>
                    <Col className="p-3">{user.email}</Col>
                </Row>
                <Row className="mr-6 mb-2 border border-dark">
                    <Col xs={2} className="p-3 bg-info text-white">Phone Number:</Col>
                    <Col className="p-3">{user.phone_number}</Col>
                </Row>
                <Row className="mr-6 mb-2 border border-dark">
                    <Col xs={2} className="p-3 bg-info text-white">Address:</Col>
                    <Col className="p-3">{user.address}</Col>
                </Row>
            </Container>
            <span style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Link to={`/update`}>Update Account details</Link>
            </span>
        </div>
    )
}

export default AccountPage
