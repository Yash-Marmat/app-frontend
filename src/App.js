import React, { useState, useEffect } from 'react'
import {useDispatch} from 'react-redux'
import IdleTimer from "react-idle-timer"
import { BrowserRouter as Router, Route } from 'react-router-dom'
import NavBar from './components/NavBar'
import RegisterPage from './components/RegisterPage'
import LoginPage from './components/LoginPage'
import AccountPage from './components/AccountPage'
import AccountUpdatePage from './components/AccountUpdatePage'
import { logout } from './actions/userActions'


function App() {
    const dispatch = useDispatch()

    // counter
    const [counter, setCounter] = useState(0);
    const [keepCounting, setKeepCounting] = useState(true);

    const onAction = (e) => {
        setCounter(0)
        setKeepCounting(true)
    };

    const onActive = (e) => {
        setCounter(0)
        setKeepCounting(true)
    };

    const onIdle = (e) => {
        //
    };

    useEffect(() => {
        if (keepCounting) {
            const interval = setInterval(() => {
                setCounter((counter) => counter + 1);
            }, 1000);

            return () => {
                clearInterval(interval);
            };
        }
    }, [keepCounting]);

    if (counter >= 120) {
        setKeepCounting(false);
        setCounter("stopped");
        dispatch(logout())
    }

    return (
        <Router>
            <IdleTimer
                // ref={ref => { Null = ref }}
                element={document}
                onActive={onActive}
                onIdle={onIdle}
                onAction={onAction}
                debounce={250}
                timeout={10}
            />
            <NavBar />
            <div className="container mt-4">
                <Route path="/" component={AccountPage} exact />
                <Route path="/update" component={AccountUpdatePage} exact />
                <Route path="/login" component={LoginPage} exact />
                <Route path="/register" component={RegisterPage} exact />
            </div>
        </Router>
    )
}

export default App