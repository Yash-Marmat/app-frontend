import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { Form, Button, Row, Col } from 'react-bootstrap'
import { register } from '../actions/userActions'


function RegisterPage({ history }) {
    const dispatch = useDispatch()

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [address, setAddress] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')

    // register reducer
    const userRegisterReducer = useSelector(state => state.userRegisterReducer)
    const { error, userInfo } = userRegisterReducer

    useEffect(() => {
        if (userInfo) {
            history.push('/')
        }
    }, [history, userInfo])

    const submitHandler = (e) => {
        e.preventDefault()
        if (password !== confirmPassword) {
            alert('Passwords do not match!')
        } else {
            dispatch(register(
                firstName,
                lastName,
                email,
                phoneNumber,
                address,
                password
            ))
        }
    }

    return (
        <div>
            <Row className='justify-content-md-center'>
                <Col xs={12} md={6}>
                    <h1>Sign Up</h1>
                    {error ? <span className = "text-danger">*Invalid Credentials*</span> : ""}   
                    <Form onSubmit={submitHandler}>

                        <Form.Group controlId='firstName'>
                            <Form.Label>
                                First Name
                        </Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="enter your first name"
                                value={firstName}
                                onChange={(e) => setFirstName(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='lastName'>
                            <Form.Label>
                                Last Name
                        </Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="enter your last name"
                                value={lastName}
                                onChange={(e) => setLastName(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='email'>
                            <Form.Label>
                                Email Address
                        </Form.Label>
                            <Form.Control
                                required
                                type="email"
                                placeholder="enter your email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='phoneNumber'>
                            <Form.Label>
                                Phone Number
                        </Form.Label>
                            <Form.Control
                                required
                                type="phone"
                                placeholder="enter your phone number"
                                value={phoneNumber}
                                onChange={(e) => setPhoneNumber(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='address'>
                            <Form.Label>
                                Address
                        </Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="enter your address"
                                value={address}
                                onChange={(e) => setAddress(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>


                        <Form.Group controlId='password'>
                            <Form.Label>
                                Password
                        </Form.Label>
                            <Form.Control
                                required
                                type="password"
                                placeholder="enter your password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='passwordConfirm'>
                            <Form.Label>
                                Confirm Password
                        </Form.Label>
                            <Form.Control
                                required
                                type="password"
                                placeholder="confirm your password"
                                value={confirmPassword}
                                onChange={(e) => setConfirmPassword(e.target.value)}
                            >
                            </Form.Control>
                        </Form.Group>

                        <Button type="submit" variant='primary'>Sign Up</Button>
                    </Form>

                    <Row className="py-3">
                        <Col>
                            Already have an account?
                            <Link
                                to={`/login`}
                            > Login</Link>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    )
}


export default RegisterPage